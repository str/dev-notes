# blockchain workshop readme - exonum

exonum - a private blockchain platform (so less secure, since is on 'private network')

https://github.com/exonum

* smart contracts
* like ethereum

5000 tps
Byzantine consensus algorithm
clearing latency 0.5s

bitcoin - 7 tps (!)

anchored to bitcoin blockchain (hashes, to prevent fraud)

---

issues:
- no schema versioning or data structure migration tools

future:
- c bindings to allow other langs, or non-REST clients
- java bindings (java more commercial than rust)
- schema versioning?

---

js client library - 'light client'

rust - back end - secure language, faster than Java or Go - very like C(++)

---

cryptoowls

https://github.com/exonum/exonum-cryptoowls
