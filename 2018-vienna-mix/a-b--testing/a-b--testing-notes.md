# a/b testing notes

2 main applications:

- deciding which design is best for users
- increasing uplift (click-thrus, registrations, cart completions...)

but NOT for settling disputes about which design/implementation is better,
  because setting up and running A/B tests can be costly

_____
## approach

- small changes can have big wins

- test A vs B
- make C an improvement on B
- test B vs C
- continue ...

_____

# tools

https://vwo.com/
$

https://www.optimizely.com
$$$
