# notes taken at the weAreDevelopers 2018 conference in Vienna

# day 1 talks I missed 

Joseph Sirosh’s keynote address offered an inspiring look into the future of AI and ML. Michael Fausten gave us a fascinating insight into the technologies behind automated driving, and Srushtika Neelakantam showed us how to build multiplayer VR web apps in just a matter of minutes. Also, the lively IoT Revolution panel discussion proved why you should definitely check out the Executive Track.

TODO post video links when they're published

https://www.wearedevelopers.com/wearedevelopers-world-congress-2018-day-1-recap/?utm_source=WeAreDevelopers+2018+Attendees+UPDATED+16.05.2018&utm_campaign=ef6a44e996-EMAIL_CAMPAIGN_2018_05_16&utm_medium=email&utm_term=0_1f0283cc61-ef6a44e996-229664729


