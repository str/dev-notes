# IBM IoT workshop - rough notes

voucher:

- see email in yahoo "IBM IoT cloud voucher"

main links:

visualize from a device:
https://quickstart.internetofthings.ibmcloud.com/#/device/wearedevelopers-05/sensor/

editor - like MFE:
https://sr-node-red-weather-1.eu-gb.mybluemix.net/red/#flow/b6de31f.42de4d

---

w/s quickstart:
https://ibm.ent.box.com/notes/292615768937?s=u7dbp49lxdjkapm75xtg51qqaf4bos1x

example dashboard:
https://wearedevelopers.mybluemix.net/ui/#/0
https://flows.nodered.org/node/node-red-dashboard

IoT home:
https://internetofthings.ibmcloud.com/#/

quickstart:
05 https://quickstart.internetofthings.ibmcloud.com/#/device/wearedevelopers-05/sensor/
06 https://quickstart.internetofthings.ibmcloud.com/#/device/wearedevelopers-06/sensor/

app getting started page:
https://console.bluemix.net/apps/c7f47aaf-073a-4086-b6da-0ec06d1ae7c0?paneId=startcoding&env_id=ibm:yp:eu-gb

app pages:
https://console.bluemix.net/apps/68839d6e-8cbf-44fb-b59f-acacd78ebc3f?paneId=logs&ace_config=%7B%22region%22%3A%22eu-gb%22%2C%22orgGuid%22%3A%22ddff4443-ea9c-4bf9-98f7-ca546b462d5c%22%2C%22spaceGuid%22%3A%221c872300-f70f-4a49-82c5-b40bc512258f%22%2C%22redirect%22%3A%22https%3A%2F%2Fconsole.bluemix.net%2Fdashboard%2Fapps%22%2C%22bluemixUIVersion%22%3A%22v6%22%2C%22crn%22%3A%22crn%3Av1%3Abluemix%3Apublic%3A%3Aeu-gb%3As%2F1c872300-f70f-4a49-82c5-b40bc512258f%3A%3Acf-application%3A68839d6e-8cbf-44fb-b59f-acacd78ebc3f%22%2C%22id%22%3A%2268839d6e-8cbf-44fb-b59f-acacd78ebc3f%22%7D&env_id=ibm:yp:eu-gb

https://console.bluemix.net/apps/68839d6e-8cbf-44fb-b59f-acacd78ebc3f?paneId=runtime&ace_config=%7B%22region%22%3A%22eu-gb%22%2C%22orgGuid%22%3A%22ddff4443-ea9c-4bf9-98f7-ca546b462d5c%22%2C%22spaceGuid%22%3A%221c872300-f70f-4a49-82c5-b40bc512258f%22%2C%22redirect%22%3A%22https%3A%2F%2Fconsole.bluemix.net%2Fdashboard%2Fapps%22%2C%22bluemixUIVersion%22%3A%22v6%22%2C%22crn%22%3A%22crn%3Av1%3Abluemix%3Apublic%3A%3Aeu-gb%3As%2F1c872300-f70f-4a49-82c5-b40bc512258f%3A%3Acf-application%3A68839d6e-8cbf-44fb-b59f-acacd78ebc3f%22%2C%22id%22%3A%2268839d6e-8cbf-44fb-b59f-acacd78ebc3f%22%7D&env_id=ibm:yp:eu-gb

app overview
https://console.bluemix.net/apps/68839d6e-8cbf-44fb-b59f-acacd78ebc3f?paneId=logs&env_id=ibm:yp:eu-gb

node-red
https://sr-node-red-weather-1.eu-gb.mybluemix.net

app editor - like MFE - but free-flow
https://sr-node-red-weather-1.eu-gb.mybluemix.net/red/#flow/b6de31f.42de4d

app dashboard!
https://sr-node-red-weather-1.eu-gb.mybluemix.net/ui/#/0

bluemix CLI download:
https://console.bluemix.net/docs/cli/reference/bluemix_cli/get_started.html#getting-started

mange db/services:
https://console.bluemix.net/services/cloudantnosqldb/0a2865cc-14da-4e29-8769-b341c3c99b04?ace_config=%7B%22region%22%3A%22eu-gb%22%2C%22orgGuid%22%3A%22ddff4443-ea9c-4bf9-98f7-ca546b462d5c%22%2C%22spaceGuid%22%3A%221c872300-f70f-4a49-82c5-b40bc512258f%22%2C%22redirect%22%3A%22https%3A%2F%2Fconsole.bluemix.net%2Fdashboard%2Fapps%22%2C%22bluemixUIVersion%22%3A%22v6%22%2C%22crn%22%3A%22crn%3Av1%3Abluemix%3Apublic%3A%3Aeu-gb%3As%2F1c872300-f70f-4a49-82c5-b40bc512258f%3A0a2865cc-14da-4e29-8769-b341c3c99b04%3Acf-service-instance%3A%22%2C%22id%22%3A%220a2865cc-14da-4e29-8769-b341c3c99b04%22%7D&env_id=ibm%3Ayp%3Aeu-gb

DBs:
https://949863d9-1675-4b00-bc49-35daf8c571db-bluemix.cloudant.com/dashboard.html#/_all_dbs

IBM Watson
https://dataplatform.ibm.com/analytics/notebooks/ecf9004e-cad9-4263-9bfb-a2c889bcbe12/view?access_token=ce6cdcc91860dd4f95e98f3b707d3b43cb4df1cd3441148b1783b3185605cf45

dataplatform (login did not work)
https://dataplatform.ibm.com/registration/steptwo?context=wdp

datascience
https://datascience.ibm.com

https://wearedevelopers.mybluemix.net/data

## techs: 
R, Jupiter (javascript in the app editor)
