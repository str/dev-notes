# learning ML

## (watched) 6 month plan:
https://youtu.be/MOdlp1d0PNA

- python / R
- MIT math courses
- book: Data science from scratch
- datasets: Iris, scikit-learn
- web scraping, APIs
- clean and categorize data fields 
- then can try ML algorithms: choose and tune
- visualize results
- start over! and iterate
- pick a passion: a domain of interest (birds? economics?)
- track your projects on github and write them up

speaker: zwmiller.com
(awesome cv site!)

## Metis - courses
https://www.thisismetis.com/explore-data-science

https://explore-data-science.thisismetis.com/about?_ga=2.143595574.2032900274.1529132939-55394347.1529132939&_gac=1.229619182.1529132939.Cj0KCQjw6pLZBRCxARIsALaaY9bmZZxjkMmirUPmBLrBkahqxE_Hy_iY2aiz9pyNdPDFjDHnaZrBoEkaAisAEALw_wcB

_____________________________
## Berkeley RL bootcamp

https://sites.google.com/view/deep-rl-bootcamp/lectures

https://github.com/pmlg/deep-rl-bootcamp

https://github.com/gwwang16/Deep-RL-Bootcamp-Labs

______________________________________
## open university Masters - too generic, like UCD? (same for the UK one)
https://www.ou.nl/?gclid=Cj0KCQjw3v3YBRCOARIsAPkLbK5IQJxN_u2yk9D_1irtt85VRve7r9dfAUkI6iGmHl1GVz_miLQ62qgaAn3AEALw_wcB

______________________________________
## various courses

https://medium.freecodecamp.org/every-single-machine-learning-course-on-the-internet-ranked-by-your-reviews-3c4a7b8026c0

** good reviews: - practical TensorFlow - but not as practical?
https://www.class-central.com/course/kadenze-creative-applications-of-deep-learning-with-tensorflow-6679
- how much does this add to the TensorFlow book ?

-ve bad reviews:
https://www.class-central.com/course/coursera-neural-networks-for-machine-learning-398

*** good one? - good reviews - Python - practical!
https://www.udemy.com/deeplearning/?siteID=SAyYsTvLiGQ-QrNiJucF2gSv0SYT6Ws9Tg&LSNPUBID=SAyYsTvLiGQ

[done!]
https://www.class-central.com/course/coursera-machine-learning-835

______________________________________
## Berkeley course - Data Science - NOT online?
Open University- Ire/UK/USA

https://extension.berkeley.edu/public/category/courseCategoryCertificateProfile.do?method=load&certificateId=28652248

$5100 + materials + registration fee
= 4300 euro

______________________________________
 