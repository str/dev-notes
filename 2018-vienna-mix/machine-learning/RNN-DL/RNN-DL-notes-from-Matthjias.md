# notes from Matthijs

## about DL

RNN readme
https://medium.com/@curiousily/making-a-predictive-keyboard-using-recurrent-neural-networks-tensorflow-for-hackers-part-v-3f238d824218

Berkeley course
https://bcourses.berkeley.edu/courses/1453965/pages/cs294-129-designing-visualizing-and-understanding-deep-neural-networks

## tools

Docker image for DL
https://github.com/floydhub/dl-docker

## various AI sites

http://deeplearning.net

eddi chatbot
https://eddi.labs.ai
https://about.me/gregor.jarisch

Keras plays catch
https://gist.github.com/EderSantana/c7222daa328f0e885093#file-qlearn-py-L157

https://buzzrobot.com/5-ways-to-get-started-with-reinforcement-learning-b96d1989c575

AI blog
https://medium.com/@curiousily
