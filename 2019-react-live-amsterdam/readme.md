# 2019: React Live (Amsterdam)

https://www.reactlive.nl/

## Design Systems

Styled System - for themes

Code Sandbox

Styled components - css in js

https://github.com/siddharthkp/react-live

npm: [styled-system](https://www.npmjs.com/package/styled-system)

npm: [@emotion/styled](https://www.npmjs.com/package/@emotion/styled)

____________

## SVG in React

Use sketch to draw svg

### svg to jsx

npm: [svgr-cli](https://www.npmjs.com/package/svgr-cli)

- also optimises the svg

### Animation

- [Pose](https://popmotion.io/pose/)

- [Framer Motion (better)](https://www.framer.com/motion/)

____________

## React Native & Ignite & Reactotron IDE

Jason Holmgren - infinite.red

[reactotron ide](https://github.com/infinitered/reactotron) - for react native, mob-state-tree

Start a new React app with [ignite](https://github.com/infinitered/ignite)

browser detector: [bowser](https://www.npmjs.com/package/bowser)

React navigation.

```<TouchableOpacity/>``` = a button!
____________

## sizzy.co browser

[overmind](https://overmindjs.org/?view=react&typescript=false) - alt to [mobx-state-tree](https://github.com/mobxjs/mobx-state-tree)

[sizzy.co](https://sizzy.co) - browser for design or dev

____________

## itests - Cypress

[Cypress](https://www.cypress.io) - for testing — itests, performance

____________

## gatsby - static site generator, based on React

[gatsby](https://www.gatsbyjs.org) static site gen uses react

[netify](https://www.netlify.com) to auto deploy site from github

____________

## Live coding react and mobx replacement! 

https://github.com/maxgallo

____________

## Next.js - app f/w on React

[Next.js](https://github.com/zeit/next.js/)

- routing
- SSR/CSR options
- [AMP standard](https://amp.dev) (used by newspapers, high perf page loading)
- api support (Web api http Web services)

uses express

____________

## React tune-up

https://bit.ly/react-tune-up


Cypress perf itest - page load time, click handling time

React-window - virtual scrolling

React : Memo

Profiler (instrument the app by wrapping component)

Styled.Image:
- respAttrs (RWD via npm package)
- lazy 

React.LazyLoad - for non chrome browser that does not have lazy image loading

https://github.com/yoavniran/reactlive2019

https://github.com/yoavniran/

____________

## Splitting bundles in React Native 

React native: js bundler is metro

`react-loadable` - lazy load js

Beta: `bundle-split` which splits metro bundle

____________

## React history (state mgt) & Hooks

https://github.com/httpJunkie

### React history:

https://dev.to/httpjunkie/react-live-resources-3j5h

https://blog.risingstack.com/the-history-of-react-js-on-a-timeline/

### React hooks:

https://github.com/httpJunkie/kr-todo-hooks

linting: `eslint-plugin-react-hooks`

useState - e.g. for click counter

useReducer - e.g. sum

useEffect - e.g. to set window title on state change
