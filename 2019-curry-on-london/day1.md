# Curry On - Day 1

CraneStation/wasmtime
https://github.com/CraneStation/wasmtime

Standalone JIT-style runtime for WebAsssembly, using Cranelift - CraneStation/wasmtime

Elements of Programming
https://books.google.nl/books/about/Elements_of_Programming.html?id=CO9ULZGINlsC&source=kp_book_description&redir_esc=y

“Ask a mechanical, structural, or electrical engineer how far they would get without a heavy reliance on a firm mathematical foundation, and they will tell you, ‘not far.’ Yet so-called software engineers often practice their art with little or no id

Paper - On The Naturalness of Software
[pdf](pdf/paper-on-the-naturalness-of-software.pdf)
[origin](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.221.1261&rep=rep1&type=pdf)

fastText
https://fasttext.cc/
Library for efficient text classification and representation learning

## Data Types, Grammers

Appendix: Algebraic Data Types in Scala | alvinalexander.com
https://alvinalexander.com/scala/fp-book/algebraic-data-types-adts-in-scala

Algebraic Data Type: “A type defined by providing several alternatives, each of which comes with its own constructor. It usually comes with a way to decompose the type through pattern matching. The concept is found in specification languages and func

https://thrift.apache.org/

antlr/grammars-v4
https://github.com/antlr/grammars-v4/tree/master/thrift

Grammars written for ANTLR v4; expectation that the grammars are free of actions. - antlr/grammars-v4

Linear grammar - Wikipedia
https://en.m.wikipedia.org/wiki/Linear_grammar

## Software Quality

- not much strong evidences that languages or stronger typing improves software quality
- biggest difference is between individual developers
- a stronger factor seems to be hours worked / how well the work is managed

### Paper (in the processed of being) discredited by Jan Vitek

https://web.cs.ucdavis.edu/~filkov/papers/lang_github.pdf

https://www.curry-on.org/2019/sessions/getting-everything-wrong-without-doing-anything-right-on-the-perils-of-large-scale-analysis-of-github-data.html

(problems with the data sourced from github - e.g. 'ts' files included, from before TypeScript was invented)

### Crunch Mode

https://www.gamasutra.com/blogs/JohanKarlsson/20190321/339135/Avoiding_Crunch_Mode_How_Studios_Can_Beat_Burnout.php

http://chadfowler.com/2014/01/22/the-crunch-mode-antipattern.html

https://www.infoq.com/news/2008/01/crunch-mode/

Why Crunch Modes Doesn't Work: Six Lessons - International Game Developers Association (IGDA)
https://www.igda.org/page/crunchsixlessons

## Companies

droit
https://droit.tech/what-we-do.html

Nextjournal
https://nextjournal.com/
A better way to write, share, remix and collaborate on your research.
Like jupyter but with version control - has imp/exp to git

Microsoft - Azure Free Account
Create your Azure free account today | Microsoft Azure
https://azure.microsoft.com/en-gb/free/?wt.mc_id=REACTOR_WW
Get started with 12 months of free services and USD 200 in credit. Create your free account today with Microsoft Azure.
