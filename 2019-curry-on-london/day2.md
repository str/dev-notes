# Curry On - Day 2

## from LOGO lady - Cynthia Solomon

https://en.wikipedia.org/wiki/Cynthia_Solomon

https://en.wikipedia.org/wiki/Logo_(programming_language)

https://turtleart.org/programming/index.html

[Logo - A Computer Language to Grow With](https://www.curry-on.org/2019/sessions/logo-a-computer-language-to-grow-with.html)

Scratch - Imagine, Program, Share
https://scratch.mit.edu/
Scratch is a free programming language and online community where you can create your own interactive stories, games, and animations.

jmoenig/Snap
https://github.com/jmoenig/Snap
a visual programming language inspired by Scratch. Contribute to jmoenig/Snap development by creating an account on GitHub.

Inventive Minds
https://mitpress.mit.edu/books/inventive-minds
Six essays by artificial intelligence pioneer Marvin Minsky on how education can foster inventiveness, paired with commentary by Minsky's former colleagues and students. Marvin Minsky was a pioneering researcher in artificial intellig

https://gtoolkit.com/

feenk
https://feenk.com/
We reshape the Development eXperience

http://humane-assessment.com/

https://elmish.github.io/elmish/

---

Audio from javascript
andrewcb/plink
https://github.com/andrewcb/plink
An AudioUnit-based music programming environment for macOS - andrewcb/plink

carp-lang/Carp
https://github.com/carp-lang/Carp
A statically typed lisp, without a GC, for real-time applications. - carp-lang/Carp

---

## `f#`

https://fsharp.org/history/

https://fable.io/

https://fsharp.org/

https://safe-stack.github.io/

Ionide - Crossplatform F# Editor Tools
http://ionide.io/
Visual Studio Code & Atom plugins for F# development

TimLariviere/FabulousContacts
https://github.com/TimLariviere/FabulousContacts
Sample contact manager app entirely written in F# and Fabulous - TimLariviere/FabulousContacts

Self-Adjusting Computation | Hacker News
https://news.ycombinator.com/item?id=9635248
