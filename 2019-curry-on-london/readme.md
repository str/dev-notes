# Curry On! London - June 2019

[Day 1](day1.md)
[Day 2](day2.md)

## programme

https://www.curry-on.org/2019/#program

[Programme as PDF](pdf/programme.pdf)
